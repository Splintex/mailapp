import angular from 'angular';
import Components from './components/components';
import AppComponent from './app.component';
import GetEmails from './components/emailList/emailList.service';


let app = angular.module("myApp", [
	Components.name
]);

app.component("app", AppComponent);
app.service('GetEmails', GetEmails);