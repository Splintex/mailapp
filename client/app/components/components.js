import angular from 'angular';
import Sidebar from './sidebar/sidebar';
import EmailList from './emailList/emailList';
//import About from './about/about';

let componentModule = angular.module('app.components', [
  Sidebar.name,
  EmailList.name
]);

export default componentModule;
