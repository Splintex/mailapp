import angular from 'angular';
import sidebarComponent from './sidebar.component';

let sidebarModule = angular.module('sidebar', []);

sidebarModule.component('sidebar', sidebarComponent);

export default sidebarModule;
