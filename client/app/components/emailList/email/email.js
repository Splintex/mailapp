import angular from 'angular';
import uiRouter from 'angular-ui-router';
import emailComponent from './email.component';

let emailModule = angular.module('email', [
  uiRouter
])

.component('email', emailComponent);

export default emailModule;
