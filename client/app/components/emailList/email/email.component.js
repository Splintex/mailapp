import template from './email.html';
import controller from './email.controller';
import './email.styl';

let emailComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default emailComponent;
