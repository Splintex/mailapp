let GetEmails = function($http){
    'ngInject';
    this.getAllUsers = function() {
        return $http.get('http://jsonplaceholder.typicode.com/users')
          .then(function(response) {
            return response.data;
          });
      };
      this.getOneUser = function(id) {
        return $http.get('http://jsonplaceholder.typicode.com/users')
          .then(function(response) {
            return response.data[id];
          });
      };

};

export default GetEmails;