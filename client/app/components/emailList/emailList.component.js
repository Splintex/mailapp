import template from './emailList.html';
import controller from './emailList.controller';
import './emailList.styl';

let emailListComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default emailListComponent;
