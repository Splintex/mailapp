import GetEmails from "./emailList.service"
class MaillistController {
  constructor(GetEmails) {
    this.name = 'emailList';
    var _this = this;
    GetEmails.getAllUsers().then(function(users) {
        _this.users = users;
        console.log(users);
    });
  }
}

export default MaillistController;
