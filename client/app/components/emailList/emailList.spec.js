// import MaillistModule from './emailList'
// import MaillistController from './emailList.controller';
// import MaillistComponent from './emailList.component';
// import MaillistTemplate from './emailList.html';

// describe('Maillist', () => {
//   let $rootScope, makeController;

//   beforeEach(window.module(MaillistModule.name));
//   beforeEach(inject((_$rootScope_) => {
//     $rootScope = _$rootScope_;
//     makeController = () => {
//       return new MaillistController();
//     };
//   }));

//   describe('Module', () => {
//     // top-level specs: i.e., routes, injection, naming
//   });

//   describe('Controller', () => {
//     // controller specs
//     it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
//       let controller = makeController();
//       expect(controller).to.have.property('name');
//     });
//   });

//   describe('Template', () => {
//     // template specs
//     // tip: use regex to ensure correct bindings are used e.g., {{  }}
//     it('has name in template [REMOVE]', () => {
//       expect(MaillistTemplate).to.match(/{{\s?vm\.name\s?}}/g);
//     });
//   });

//   describe('Component', () => {
//       // component/directive specs
//       let component = MaillistComponent;

//       it('includes the intended template',() => {
//         expect(component.template).to.equal(MaillistTemplate);
//       });

//       it('uses `controllerAs` syntax', () => {
//         expect(component).to.have.property('controllerAs');
//       });

//       it('invokes the right controller', () => {
//         expect(component.controller).to.equal(MaillistController);
//       });
//   });
// });
