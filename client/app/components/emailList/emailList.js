import angular from 'angular';
import uiRouter from 'angular-ui-router';
import emailListComponent from './emailList.component';

let emailListModule = angular.module('emailList', [])

.component('maillist', emailListComponent);

export default emailListModule;
